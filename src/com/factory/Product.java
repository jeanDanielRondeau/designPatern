package com.factory;

public abstract class Product {
	public abstract Product createProduct();
}
