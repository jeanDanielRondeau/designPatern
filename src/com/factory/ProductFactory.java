package com.factory;

import java.util.HashMap;

public class ProductFactory{
	
	private static ProductFactory instance = new ProductFactory();
	private HashMap<Integer, Product> registeredProducts = new HashMap<Integer, Product>();
	
	private ProductFactory() {
	}
	public static ProductFactory getInstance() {
		return instance;
	}
	
	public Product createProduct(Class<?> type){
		Product product = null;
		if (type.equals(USB.class)) {
			product = new USB();
		}
		else if (type.equals(Computer.class)) {
			product = new Computer();
		}
		
		if(product != null) {
			registeredProducts.put(registeredProducts.size(), product);
		}
        return product; 
    }
	
	public Product getProductById(int key) {
		return registeredProducts.get(key);
	}
}
