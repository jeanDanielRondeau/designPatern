package com.bridge;

public interface MediaPlayer {
	public void play(String audioType, String fileName);
}
