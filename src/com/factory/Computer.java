package com.factory;

public class Computer extends Product{
	protected Computer() {
	}

	@Override
	public Product createProduct() {
		return new Computer();
	}
}
