package com.factory;

public class Main {
	public static void main(String[] args) {
		ProductFactory productFactory = ProductFactory.getInstance(); 
		Product usb = productFactory.createProduct(USB.class);
		USB usb2 = (USB)productFactory.createProduct(USB.class);
		Product computer = productFactory.createProduct(Computer.class);
		Product nullObject = productFactory.createProduct(Product.class);
		System.out.println("usb: "+usb.getClass());
		System.out.println("usb2: "+usb2.getClass());
		System.out.println("computer: "+computer.getClass());
		if(nullObject != null) {
			System.out.println("nullObject: "+nullObject.getClass());
		}
		
		System.out.println("------------get product by ids--------------");
		System.out.println(productFactory.getProductById(0).getClass());
		System.out.println(productFactory.getProductById(1).getClass());
		System.out.println(productFactory.getProductById(2).getClass());
	}

}
