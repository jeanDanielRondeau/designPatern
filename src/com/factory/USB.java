package com.factory;

public class USB extends Product{
	protected USB() {
	}
	@Override
	public Product createProduct() {
		return new USB();
	}
}
